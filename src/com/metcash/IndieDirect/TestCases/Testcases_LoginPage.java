package com.metcash.IndieDirect.TestCases;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.metcash.IndieDirect.Genericfunctions.GenericFunctions;
import com.metcash.IndieDirect.Genericfunctions.URLProvider;
import com.metcash.IndieDirect.Genericfunctions.Xls_Reader;
import com.metcash.IndieDirect.PageObjects.LoginPage;


public class Testcases_LoginPage  {
	String browserType = "Chrome";
	WebDriver driver;
	GenericFunctions generic;

	@BeforeMethod
	@Parameters({ "browserType" })
	public void Before(){
		generic = new GenericFunctions(driver);
		driver = generic.StartDriver(browserType);
		driver.manage().window().maximize();
	}
	@AfterTest()
	public void aftertest()
	{
		driver.quit();
	}
	//@Test
	//To verify the login functionality 
	public void TC01()
	{
		driver.get(URLProvider.Return_LoginlURL);
		driver.findElement(By.xpath(LoginPage.username_txt)).sendKeys("customer1@test.com");
		driver.findElement(By.xpath(LoginPage.password_txt)).sendKeys("12341234");
		driver.findElement(By.xpath(LoginPage.login_btn)).click();
	}
	
	
	
	@Test
	public void TC02()
	{
		Xls_Reader datatable = new Xls_Reader("C:\\workspace\\Sample_IndieDirect\\TestCaseCreation\\Sample.xlsx");
		System.out.println(datatable.getCellData("Login Page", "username", 2));
		System.out.println(datatable.getCellData("Login Page", "username", 5));

		
	}
	
}


