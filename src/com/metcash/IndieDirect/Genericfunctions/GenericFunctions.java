package com.metcash.IndieDirect.Genericfunctions;

import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

public class GenericFunctions {
	public WebDriver driver;

	public GenericFunctions(WebDriver driver) {
		this.driver = driver;
	}

	public WebDriver StartDriver(String browserType) {

		if (browserType.trim().equalsIgnoreCase("")) {
			System.out.println("Kindly set the 'browserType' variable before calling this function");
			// System.setProperty("webdriver.gecko.driver",
			// "C:\\Common_Resources\\geckodriver.exe");
			driver = new FirefoxDriver();

		}

		if (browserType.equalsIgnoreCase("firefox")) {
			System.setProperty("webdriver.gecko.driver", "C:\\Common_Resources\\geckodriver.exe");
			driver = new FirefoxDriver();

		}

		else if (browserType.startsWith("Chrome")) {
			System.setProperty("webdriver.chrome.driver", "C:\\Common_Resources\\chromedriver.exe");
			driver = new ChromeDriver();

		} else if (browserType.startsWith("IE")) {
			System.setProperty("webdriver.ie.driver", "C:\\Common_Resources\\IEDriverServer.exe");
			driver = new InternetExplorerDriver();

		}
		return driver;
	}

	public String SetImplicitWaitInSeconds(int timeOut) {
		driver.manage().timeouts().implicitlyWait(timeOut, TimeUnit.SECONDS);
		return "Timeout set to " + timeOut + " seconds.";
	}

	public String AlertBox_Accept() {
		// Get a handle to the open alert, prompt or confirmation
		Alert alert = driver.switchTo().alert();
		// And acknowledge the alert (equivalent to clicking "OK")
		alert.accept();
		return ("Alert '" + alert.getText() + "' accepted");
	}

	public String AlertBox_Dismiss() {
		// Get a handle to the open alert, prompt or confirmation
		Alert alert = driver.switchTo().alert();
		// And acknowledge the alert (equivalent to clicking "cancel")
		alert.dismiss();
		return ("Alert '" + alert.getText() + "' dismissed");
	}

	public void CloseNewWindow() {
		driver.close();
	}

	public void GoToSleep(int TimeInMillis) {
		try {
			Thread.sleep(TimeInMillis);
		} catch (Exception e) {
		}
	}

	public String fnCheckSelectedValueOfCalender(String dateonexcel, String inputdateonCalender) {
		String result = "";
		String ActualDatetextOnCalender = inputdateonCalender;
		String ActualDateTextOnInput = dateonexcel;
		String dateoncinput;
		String monthoninput;
		String yearoninput;
		String formatteddatofinput;

		Hashtable<String, String> months = new Hashtable<String, String>();
		months.put("Jan", "01");
		months.put("Feb", "02");
		months.put("Mar", "03");
		months.put("Apr", "04");
		months.put("May", "05");
		months.put("Jun", "06");
		months.put("Jul", "07");
		months.put("Aug", "08");
		months.put("Sep", "09");
		months.put("Oct", "10");
		months.put("Nov", "11");
		months.put("Dec", "12");

		dateoncinput = ActualDateTextOnInput.substring(0, ActualDateTextOnInput.indexOf("-"));
		if (dateoncinput.length() == 1) {
			dateoncinput = "0" + dateoncinput;
		}
		monthoninput = ActualDateTextOnInput.substring(ActualDateTextOnInput.indexOf("-") + 1,
				ActualDateTextOnInput.indexOf("-") + 4);
		yearoninput = ActualDateTextOnInput.substring(ActualDateTextOnInput.lastIndexOf("-") + 1,
				ActualDateTextOnInput.length());
		monthoninput = months.get(monthoninput);

		formatteddatofinput = yearoninput + "-" + monthoninput + "-" + dateoncinput;
		System.out.println(formatteddatofinput);
		if (ActualDatetextOnCalender.equals(formatteddatofinput)) {
			result = "Pass";
		} else {
			result = "Fail";
		}

		return result;

	}

	// ================== Select Dates in Calender =============
	public void fnSelectDatefromCalender(String CalIconId, String calId, String dateToSelect) {

		// String result="";

		// int date=Integer.parseInt(dateToSelect.substring(0, 2));
		String[] s = dateToSelect.split("-");
		String date = s[0];
		// int year=Integer.parseInt(dateToSelect.substring(6,
		// dateToSelect.length()));
		int year = Integer.parseInt(s[2]);
		WebElement calContainer;
		@SuppressWarnings("unused")
		Date currentdate;
		List<WebElement> AllRows;
		String calenderMonth;
		int calenderYear;

		Hashtable<String, Integer> months = new Hashtable<String, Integer>();
		months.put("Jan", 1);
		months.put("Feb", 2);
		months.put("Mar", 3);
		months.put("Apr", 4);
		months.put("May", 5);
		months.put("Jun", 6);
		months.put("Jul", 7);
		months.put("Aug", 8);
		months.put("Sep", 9);
		months.put("Oct", 10);
		months.put("Nov", 11);
		months.put("Dec", 12);
		int month = months.get(s[1]);
		// System.out.println(month);
		int i = 1;
		// DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		currentdate = Calendar.getInstance().getTime();
		// System.out.println(dateFormat.format(currentdate));

		driver.findElement(By.id(CalIconId)).click();
		calContainer = driver.findElement(By.xpath("//div[@id='" + calId + "']/table/tbody"));
		AllRows = calContainer.findElements(By.xpath("./tr"));
		int numberofrows = AllRows.size();

		for (int a = 1; a <= numberofrows; a++) {
			if (i == 1) {
				int yeardiff;
				int monthdiff;
				String calderData = driver
						.findElement(By.xpath("//div[@id='" + calId + "']/table/tbody/tr[" + a + "]/td[3]")).getText();
				System.out.println(calderData);
				calenderMonth = calderData.substring(0, 3);
				System.out.println(calenderMonth);
				int calenderindex = months.get(calenderMonth);
				calenderYear = Integer.parseInt(calderData.substring(4, calderData.length()));
				System.out.println(calenderYear);
				if (calenderYear > year) {
					yeardiff = calenderYear - year;
					System.out.println("year diff is : " + yeardiff);
					for (int j = 0; j < yeardiff; j++) {
						driver.findElement(By.xpath("//div[@id='" + calId + "']/table/tbody/tr[" + a + "]"))
								.findElement(By.linkText("<<")).click();
					}
					if (month > calenderindex) {
						monthdiff = month - calenderindex;
						System.out.println("month diff is " + monthdiff);
						for (int k = 0; k < monthdiff; k++) {
							driver.findElement(By.xpath("//div[@id='" + calId + "']/table/tbody/tr[" + a + "]"))
									.findElement(By.linkText(">")).click();
						}
					} else if (month < calenderindex) {
						monthdiff = calenderindex - month;
						System.out.println("month diff is " + monthdiff);
						for (int k = 0; k < monthdiff; k++) {
							driver.findElement(By.xpath("//div[@id='" + calId + "']/table/tbody/tr[" + a + "]"))
									.findElement(By.linkText("<")).click();
						}
					}
				} else if (calenderYear < year) {
					yeardiff = year - calenderYear;
					for (int j = 0; j < yeardiff; j++) {
						driver.findElement(By.xpath("//div[@id='" + calId + "']/table/tbody/tr[" + a + "]"))
								.findElement(By.linkText(">>")).click();
					}

					if (month > calenderindex) {
						monthdiff = month - calenderindex;
						System.out.println("month diff is " + monthdiff);
						for (int k = 0; k < monthdiff; k++) {
							driver.findElement(By.xpath("//div[@id='" + calId + "']/table/tbody/tr[" + a + "]"))
									.findElement(By.linkText(">")).click();
						}
					} else if (month < calenderindex) {
						monthdiff = calenderindex - month;
						System.out.println("month diff is " + monthdiff);
						for (int k = 0; k < monthdiff; k++) {
							driver.findElement(By.xpath("//div[@id='" + calId + "']/table/tbody/tr[" + a + "]"))
									.findElement(By.linkText("<")).click();
						}
					}

				} else {

					if (month > calenderindex) {
						monthdiff = month - calenderindex;
						System.out.println("month diff is " + monthdiff);
						for (int k = 0; k < monthdiff; k++) {
							driver.findElement(By.xpath("//div[@id='" + calId + "']/table/tbody/tr[" + a + "]"))
									.findElement(By.linkText(">")).click();
						}
					} else if (month < calenderindex) {
						monthdiff = calenderindex - month;
						System.out.println("month diff is " + monthdiff);
						for (int k = 0; k < monthdiff; k++) {
							driver.findElement(By.xpath("//div[@id='" + calId + "']/table/tbody/tr[" + a + "]"))
									.findElement(By.linkText("<")).click();
						}
					}

				}

				System.out.println(months.get(calenderMonth));

			} else if (i > 2) {
				driver.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);
				if (driver.findElement(By.xpath("//div[@id='" + calId + "']/table/tbody/tr[" + a + "]"))
						.findElements(By.linkText(date)).size() != 0)

				{
					driver.findElement(By.xpath("//div[@id='" + calId + "']/table/tbody/tr[" + a + "]"))
							.findElement(By.linkText(date)).click();
					break;
				}
			}

			i = i + 1;
		}

		// return result;
	}

	public void PerformMouseHover(String xpath) {

		Actions builder = new Actions(driver);
		builder.moveToElement(driver.findElement(By.xpath(xpath)));
		builder.perform();
	}

	public void Fill(String xpath, String Value_To_Fill)

	{
		driver.findElement(By.xpath(xpath)).sendKeys(Value_To_Fill);
	}

	public String GetText(String xpath) {
		return driver.findElement(By.xpath(xpath)).getText();
	}

	public void Select(String xpath, String Value_To_Select)

	{
		System.out.println(Value_To_Select);
		new Select(driver.findElement(By.xpath(xpath))).selectByVisibleText(Value_To_Select);
		/*
		 * driver.findElement(By.xpath(xpath)).click();
		 * driver.findElement(By.xpath(xpath)).findElement(By.xpath(
		 * "//option[contains(text(),Value_To_Select)]")).click();
		 * ////a[contains(text(),'Credit Suisse')]"
		 */ }

	public void Click(String xpath)

	{
		driver.findElement(By.xpath(xpath)).click();
	}

	// ========== Date ========

	public String CheckDate(String Date, String CurrentDate) {
		String result = "Pass";
		// String a="7-Nov-2013";
		String[] a = Date.split("-");
		String date1 = a[0];
		String month1 = a[1];
		String year1 = a[2];
		if (date1.length() == 1) {
			date1 = "0" + date1;
		}

		String[] b = CurrentDate.split("-");
		String date2 = b[0];
		String month2 = b[1];
		String year2 = b[2];

		if (!date1.equals(date2)) {
			System.out.println("In Date");
			result = "Fail";
		}
		if (!month1.equals(month2)) {
			System.out.println("In Month");
			result = "Fail";
		}
		if (!(year1.equals(year2))) {
			System.out.println("In Year");
			result = "Fail";
		}
		/*
		 * System.out.println("Date1 :" + date1); System.out.println("Month1 :"
		 * + month1); System.out.println("Year1 :" + year1);
		 * System.out.println("Date2 :" + date2); System.out.println("Month2 :"
		 * + month2); System.out.println("Year2 :" + year2);
		 */
		return result;
	}

	// Function Description : Scroll page to bottom in slow motion.

	public void PageScrollToBottomInSlowMotion() {
		for (int count = 0;; count++) {
			if (count >= 5) { // count value can be changed depending on number
								// of times you want to scroll
				break;
			}
			((JavascriptExecutor) driver).executeScript("window.scrollBy(0,400)", ""); // y
																						// value
																						// '400'
																						// can
																						// be
																						// changed
			try {
				Thread.sleep(2000);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	// * Function Description : Click an element using Javascript:

	public void click_WebElementInChrome(WebElement elementToBeClicked) {
		((JavascriptExecutor) driver).executeScript("window.scrollTo(0," + elementToBeClicked.getLocation().y + ")");
		elementToBeClicked.click();
	}

	// * Function Description : MoveSlider - Function to move slider from one
	// position to other.

	public void moveSlider(WebElement eleToSlide, int xAxis, int yAxis) {
		Actions act = new Actions(driver);
		act.dragAndDropBy(eleToSlide, xAxis, yAxis).build().perform();
	}

	public String Fill_Txt(String xpath, String data) {
		driver.findElement(By.xpath(xpath)).clear();
		driver.findElement(By.xpath(xpath)).sendKeys(data);
		return "Element entered in text box successfully";
	}

	public String Click_Txt(String xpath) {
		driver.findElement(By.xpath(xpath)).click();
		return "Clicked on Text field";
	}

	// * Function Description:To mouse hover on a WebElement Function
	// parameters:

	public void MouseHover(String xpath) {
		Actions action = new Actions(driver);
		WebElement we = driver.findElement(By.xpath(xpath));
		action.moveToElement(we).build().perform();
		GoToSleep(2000);
	}

	// * Function Description:To scroll the page Function parameters: String
	// xpath
	public void Scroll_The_Page(String xpath) {
		Point loc = driver.findElement(By.xpath(xpath)).getLocation();
		// System.out.println(loc);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("javascript:window.scrollBy(0," + loc.y + ")");
	}

}
